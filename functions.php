<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php', // Theme customizer
  'lib/wp_bootstrap_navwalker.php'
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

function event_post() {

  $labels = array(
    'name'               => _x( 'Events', 'post type general name' ),
    'singular_name'      => _x( 'Event', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'event' ),
    'add_new_item'       => __( 'Add New Event' ),
    'edit_item'          => __( 'Edit Event' ),
    'new_item'           => __( 'New Event' ),
    'all_items'          => __( 'All Events' ),
    'view_item'          => __( 'View Event' ),
    'search_items'       => __( 'Search Events' ),
    'not_found'          => __( 'No events found' ),
    'not_found_in_trash' => __( 'No events found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Events'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our events and event specific data',
    'public'        => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'rewrite' => array( 'with_front' => false ),
    'menu_icon'     => 'dashicons-clock',
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
    'has_archive'   => true,
  );

  register_post_type( 'events', $args );

}
add_action( 'init', 'event_post' );

// Service Teams CPT

function serve_post() {

  $labels = array(
    'name'               => _x( 'Service Teams', 'post type general name' ),
    'singular_name'      => _x( 'Service Team', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'team' ),
    'add_new_item'       => __( 'Add New Team' ),
    'edit_item'          => __( 'Edit Team' ),
    'new_item'           => __( 'New Team' ),
    'all_items'          => __( 'All Teams' ),
    'view_item'          => __( 'View Team' ),
    'search_items'       => __( 'Search Teams' ),
    'not_found'          => __( 'No service teams found' ),
    'not_found_in_trash' => __( 'No service teams found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Service Teams'
  );
  $args = array(
    'labels'        => $labels,
    'public'        => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'rewrite' => array( 'with_front' => false ),
    'menu_icon'     => 'dashicons-groups',
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
    'has_archive'   => true,
  );

  register_post_type( 'serve', $args );

}
add_action( 'init', 'serve_post' );

function community_post() {

  $labels = array(
    'name'               => _x( 'Community Groups', 'post type general name' ),
    'singular_name'      => _x( 'Community Group', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'group' ),
    'add_new_item'       => __( 'Add New Group' ),
    'edit_item'          => __( 'Edit Group' ),
    'new_item'           => __( 'New Group' ),
    'all_items'          => __( 'All Groups' ),
    'view_item'          => __( 'View Group' ),
    'search_items'       => __( 'Search Groups' ),
    'not_found'          => __( 'No groups found' ),
    'not_found_in_trash' => __( 'No groups found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Groups'
  );
  $args = array(
    'labels'        => $labels,
    'public'        => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'rewrite' => array( 'with_front' => false ),
    'menu_icon'     => 'dashicons-admin-multisite',
    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
    'has_archive'   => true,
  );

  register_post_type( 'groups', $args );

}
add_action( 'init', 'community_post' );

add_image_size( 'list-view', 365, 320, array( 'center', 'center' ) );

add_filter( 'get_the_archive_title', function ($title) {
  if ( is_category() ) {
    $title = single_cat_title( '', false );
  } elseif ( is_tag() ) {
    $title = single_tag_title( '', false );
  } elseif ( is_post_type_archive() ) {
    $title = post_type_archive_title( '', false );
  } elseif ( is_author() ) {
    $title = '<span class="vcard">' . get_the_author() . '</span>' ;
  }
    return $title;
});


/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'yourprefix_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Demo_CMB2
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */

if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
  require_once dirname( __FILE__ ) . '/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
  require_once dirname( __FILE__ ) . '/CMB2/init.php';
}


add_action( 'cmb2_admin_init', 'event_details_metaboxes' );
/**
 * Define the metabox and field configurations.
 */
function event_details_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_event_details_';

    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'event_details',
        'title'         => __( 'Event Details', 'cmb2' ),
        'object_types'  => array( 'events', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );

    $cmb->add_field( array(
        'name' => 'Event Date & Time',
        'id'   => $prefix . 'date',
        'type' => 'text_datetime_timestamp',
    ) );

    $cmb->add_field( array(
        'name'       => __( 'Link Text', 'cmb2' ),
        'desc'       => __( 'enter text to be displayed on event link', 'cmb2' ),
        'default' => 'Get Details',
        'id'         => $prefix . 'link_text',
        'type'       => 'text',
    ) );
}

add_action( 'cmb2_admin_init', 'home_options_metaboxes' );
/**
 * Define the metabox and field configurations.
 */
function home_options_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_home_options_';

    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'home_options',
        'title'         => __( 'Homepage Options', 'cmb2' ),
        'object_types'  => array( 'page', ), // Post type
        'show_on'      => array( 'key' => 'page-template', 'value' => 'template-homepage.php' ),
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );

    $cmb->add_field( array(
        'name' => 'Hero Image',
        'desc'       => __( 'Background image for hero section', 'cmb2' ),
        'id'   => $prefix . 'hero_image',
        'type' => 'file',
    ) );

    $cmb->add_field( array(
        'name' => 'Sermon Series Image',
        'desc'       => __( 'Image for current sermon series section', 'cmb2' ),
        'id'   => $prefix . 'sermon_image',
        'type' => 'file',
    ) );

    $cmb->add_field( array(
        'name' => __( 'Sermon Series Link', 'cmb2' ),
        'desc'       => __( 'URL for sermon series link', 'cmb2' ),
        'id'   => $prefix . 'sermon_link',
        'type' => 'text_url',
    ) );

    $cmb->add_field( array(
        'name' => __( 'Sermon Series Link Text', 'cmb2' ),
        'desc'       => __( 'Text for sermon series link', 'cmb2' ),
        'default' => 'Hear the Current Sermon Series',
        'id'   => $prefix . 'sermon_link_text',
        'type' => 'text',
    ) );

    $cmb->add_field( array(
        'name' => __( 'Community Image', 'cmb2' ),
        'desc'       => __( 'Background image for the Community section', 'cmb2' ),
        'id'   => $prefix . 'community_image',
        'type' => 'file',
    ) );

    $cmb->add_field( array(
        'name' => __( 'Community Content', 'cmb2' ),
        'desc'       => __( 'Content for the Community section', 'cmb2' ),
        'id'   => $prefix . 'community_content',
        'type' => 'wysiwyg',
        'options' => array( 'wpautop' => true ),
    ) );
}

add_action( 'cmb2_admin_init', 'page_options_metaboxes' );
/**
 * Define the metabox and field configurations.
 */
function page_options_metaboxes() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_page_options_';

    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'page_options',
        'title'         => __( 'Page Options', 'cmb2' ),
        'object_types'  => array( 'page', ), // Post type
        'show_on'      => array( 'key' => 'page-template', 'value' => 'template-fullwidth.php' ),
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );

    $cmb->add_field( array(
        'name' => 'Page Title Image',
        'desc'       => __( 'Background image for page title', 'cmb2' ),
        'id'   => $prefix . 'title_image',
        'type' => 'file',
    ) );
}

add_action( 'cmb2_admin_init', 'summary_page_metaboxes' );
/**
 * Define the metabox and field configurations.
 */
function summary_page_metaboxes() {
    $prefix = '_summary_page_';
    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'summary_page',
        'title'         => __( 'Summary Page', 'cmb2' ),
        'object_types'  => array( 'page', ), // Post type
        'show_on'      => array( 'key' => 'page-template', 'value' => 'template-summary.php' ),
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );

    $cmb->add_field( array(
        'name' => 'Section 1 Image',
        'id'   => $prefix . 'section_1_image',
        'type' => 'file',
    ) );

    $cmb->add_field( array(
        'name'    => 'Section 1 Content',
        'id'      => $prefix . 'section_1_content',
        'type'    => 'wysiwyg',
    ) );

    $cmb->add_field( array(
        'name'    => 'Section 2 Content',
        'id'      => $prefix . 'section_2_content',
        'type'    => 'wysiwyg',
    ) );

    $cmb->add_field( array(
        'name' => 'Section 3 Image',
        'id'   => $prefix . 'section_3_image',
        'type' => 'file',
    ) );

    $cmb->add_field( array(
        'name'    => 'Section 3 Content',
        'id'      => $prefix . 'section_3_content',
        'type'    => 'wysiwyg',
    ) );

    $cmb->add_field( array(
        'name'    => 'Section 4 Content',
        'id'      => $prefix . 'section_4_content',
        'type'    => 'wysiwyg',
    ) );

    $cmb->add_field( array(
        'name' => 'Section 5 Image',
        'id'   => $prefix . 'section_5_image',
        'type' => 'file',
    ) );

    $cmb->add_field( array(
        'name'    => 'Section 5 Content',
        'id'      => $prefix . 'section_5_content',
        'type'    => 'wysiwyg',
    ) );

    $cmb->add_field( array(
        'name'    => 'Section 6 Content',
        'id'      => $prefix . 'section_6_content',
        'type'    => 'wysiwyg',
    ) );

    $cmb->add_field( array(
        'name' => 'Section 7 Image',
        'id'   => $prefix . 'section_7_image',
        'type' => 'file',
    ) );

    $cmb->add_field( array(
        'name'    => 'Section 7 Content',
        'id'      => $prefix . 'section_7_content',
        'type'    => 'wysiwyg',
    ) );

    $cmb->add_field( array(
        'name'    => 'Section 8 Content',
        'id'      => $prefix . 'section_8_content',
        'type'    => 'wysiwyg',
    ) );

    $cmb->add_field( array(
        'name' => 'Section 9 Image',
        'id'   => $prefix . 'section_9_image',
        'type' => 'file',
    ) );

    $cmb->add_field( array(
        'name'    => 'Section 9 Content',
        'id'      => $prefix . 'section_9_content',
        'type'    => 'wysiwyg',
    ) );

    $cmb->add_field( array(
        'name'    => 'Section 10 Content',
        'id'      => $prefix . 'section_10_content',
        'type'    => 'wysiwyg',
    ) );
}

/**
 * Hide editor on template-summary.php pages.
 *
 */
add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
  // Get the Post ID.
  $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
  if( !isset( $post_id ) ) return;
  // Hide the editor on a page with a specific page template
  // Get the name of the Page Template file.
  $template_file = get_post_meta($post_id, '_wp_page_template', true);
  if($template_file == 'template-summary.php'){ // the filename of the page template
    remove_post_type_support('page', 'editor');
  }
}


/////////////////////

/**
 * CMB2 Theme Options
 * @version 0.1.0
 */
class vl_Admin {
    /**
     * Option key, and option page slug
     * @var string
     */
    private $key = 'vl_options';
    /**
     * Options page metabox id
     * @var string
     */
    private $metabox_id = 'vl_option_metabox';
    /**
     * Options Page title
     * @var string
     */
    protected $title = '';
    /**
     * Options Page hook
     * @var string
     */
    protected $options_page = '';
    /**
     * Holds an instance of the object
     *
     * @var vl_Admin
     **/
    private static $instance = null;
    /**
     * Constructor
     * @since 0.1.0
     */
    private function __construct() {
        // Set our title
        $this->title = __( 'Site Options', 'vl' );
    }
    /**
     * Returns the running object
     *
     * @return vl_Admin
     **/
    public static function get_instance() {
        if( is_null( self::$instance ) ) {
            self::$instance = new vl_Admin();
            self::$instance->hooks();
        }
        return self::$instance;
    }
    /**
     * Initiate our hooks
     * @since 0.1.0
     */
    public function hooks() {
        add_action( 'admin_init', array( $this, 'init' ) );
        add_action( 'admin_menu', array( $this, 'add_options_page' ) );
        add_action( 'cmb2_admin_init', array( $this, 'add_options_page_metabox' ) );
    }
    /**
     * Register our setting to WP
     * @since  0.1.0
     */
    public function init() {
        register_setting( $this->key, $this->key );
    }
    /**
     * Add menu options page
     * @since 0.1.0
     */
    public function add_options_page() {
        $this->options_page = add_menu_page( $this->title, $this->title, 'manage_options', $this->key, array( $this, 'admin_page_display' ) );
        // Include CMB CSS in the head to avoid FOUC
        add_action( "admin_print_styles-{$this->options_page}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );
    }
    /**
     * Admin page markup. Mostly handled by CMB2
     * @since  0.1.0
     */
    public function admin_page_display() {
        ?>
        <div class="wrap cmb2-options-page <?php echo $this->key; ?>">
            <h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
            <?php cmb2_metabox_form( $this->metabox_id, $this->key ); ?>
        </div>
        <?php
    }
    /**
     * Add the options metabox to the array of metaboxes
     * @since  0.1.0
     */
    function add_options_page_metabox() {
        // hook in our save notices
        add_action( "cmb2_save_options-page_fields_{$this->metabox_id}", array( $this, 'settings_notices' ), 10, 2 );
        $cmb = new_cmb2_box( array(
            'id'         => $this->metabox_id,
            'hookup'     => false,
            'cmb_styles' => false,
            'show_on'    => array(
                // These are important, don't remove
                'key'   => 'options-page',
                'value' => array( $this->key, )
            ),
        ) );
        // Set our CMB2 fields
        $cmb->add_field( array(
            'name' => __( 'Site Title Bold', 'vl' ),
            'desc' => __( 'Enter site title to be bold (ex. ValleyLife)', 'vl' ),
            'id'   => 'site_title_bold',
            'type' => 'text',
            'default' => 'ValleyLife',
        ) );
        $cmb->add_field( array(
            'name' => __( 'Site Title Light', 'vl' ),
            'desc' => __( 'Enter site title to be light (ex. Church)', 'vl' ),
            'id'   => 'site_title_light',
            'type' => 'text',
            'default' => 'Church',
        ) );
        $cmb->add_field( array(
            'name' => __( 'Footer Address', 'vl' ),
            'desc' => __( 'Enter church address', 'vl' ),
            'id'   => 'site_church_address',
            'type' => 'text',
            'default' => '34406 North 27th Drive, Phoenix, AZ 85085',
        ) );
        $cmb->add_field( array(
            'name' => __( 'Footer Phone Number', 'vl' ),
            'desc' => __( 'Enter church phone number', 'vl' ),
            'id'   => 'site_phone_number',
            'type' => 'text',
            'default' => '623-850-8777',
        ) );
    }
    /**
     * Register settings notices for display
     *
     * @since  0.1.0
     * @param  int   $object_id Option key
     * @param  array $updated   Array of updated fields
     * @return void
     */
    public function settings_notices( $object_id, $updated ) {
        if ( $object_id !== $this->key || empty( $updated ) ) {
            return;
        }
        add_settings_error( $this->key . '-notices', '', __( 'Settings updated.', 'vl' ), 'updated' );
        settings_errors( $this->key . '-notices' );
    }
    /**
     * Public getter method for retrieving protected/private variables
     * @since  0.1.0
     * @param  string  $field Field to retrieve
     * @return mixed          Field value or exception is thrown
     */
    public function __get( $field ) {
        // Allowed fields to retrieve
        if ( in_array( $field, array( 'key', 'metabox_id', 'title', 'options_page' ), true ) ) {
            return $this->{$field};
        }
        throw new Exception( 'Invalid property: ' . $field );
    }
}
/**
 * Helper function to get/return the vl_Admin object
 * @since  0.1.0
 * @return vl_Admin object
 */
function vl_admin() {
    return vl_Admin::get_instance();
}
/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string  $key Options array key
 * @return mixed        Option value
 */
function vl_get_option( $key = '' ) {
    return cmb2_get_option( vl_admin()->key, $key );
}
// Get it started
vl_admin();
