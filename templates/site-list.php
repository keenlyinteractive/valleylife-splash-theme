<ul class="dropdown-menu" aria-labelledby="location">
  <?php
  $args = array(
      'network_id' => null,
      'public'     => null,
      'archived'   => null,
      'mature'     => null,
      'spam'       => null,
      'deleted'    => null,
      'limit'      => 100,
      'offset'     => 1,
  );
  $blog_list = wp_get_sites( $args );
  foreach ($blog_list AS $blog) {
    $blog_details = get_blog_details($blog['blog_id']);
  echo '<li><a href="//'.$blog['domain'].$blog['path'].'">'.$blog_details->blogname.'</a></li>';
  } ?>
</ul>
