<header class="banner navbar navbar-default navbar-static-top container" role="banner">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo-mark.png"><span class="brand-bold"><?php echo vl_get_option( 'site_title_bold' ) ?></span><span><?php echo vl_get_option( 'site_title_light' ) ?></span></a>
    <div class="select-church">
      <div class="dropdown">
      <button id="location" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Change Location <span class="caret"></span>
      </button>
      <?php get_template_part('templates/site', 'list'); ?>
      </div>
    </div>
  </div>

  <nav class="collapse navbar-collapse" role="navigation">
    <div class="dropdown-location">
      <button id="location" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Change Location <span class="caret"></span>
      </button>
      <?php get_template_part('templates/site', 'list'); ?>
    </div>
    <?php
    if (has_nav_menu('primary_navigation')) :
      wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new wp_bootstrap_navwalker(), 'menu_class' => 'nav navbar-nav']);
    endif;
    ?>
  </nav>
</header>
