<footer class="content-info container">
	<div class="contact-info">
		<div class="footer-logo">
		  	<?php if ( is_page_template( 'template-splash.php' ) ) { ?>
		  		<a href="<?= esc_url(home_url('/')); ?>"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo-mark.png"><span class="brand-bold">ValleyLife</span><span>Church</span></a>
		  	<?php } else { ?>
		  	  	<a href="<?= esc_url(home_url('/')); ?>"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo-mark.png"><span class="brand-bold"><?php echo vl_get_option( 'site_title_bold' ) ?></span><span><?php echo vl_get_option( 'site_title_light' ) ?></span></a>
			<?php }?>
		</div>
	  	<span class="tagline">Make Disciples. Plant Churches.</span>
	  	<span class="contact"><?php echo vl_get_option( 'site_church_address' ) ?><br><?php echo vl_get_option( 'site_phone_number' ) ?><br><a href="mailto:info@valleylife.church?Subject=Website%20Contact" target="_top">Email Us</a><br>©2016 Valley Life Church</span>
  	</div>
  	<?php if ( is_page_template( 'template-splash.php' ) ) {
  	} else { ?>
  	  	<div class="select-church">
	  		<div class="dropdown">
				<button id="location" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Change Location <span class="caret"></span>
				</button>
				<?php get_template_part('templates/site', 'list'); ?>
	  		</div>
		</div>
	<?php }?>
</footer>
