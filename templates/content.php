<article class="list-view">
  <?php the_post_thumbnail( 'list-view' ); ?>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
  <?php if ( is_post_type_archive( 'serve' ) ) { ?>
    <a href="<?php the_permalink(); ?>" class="btn btn-primary main-link">Join Team</a>
  <?php } elseif ( is_post_type_archive( 'groups' ) ) { ?>
      <a href="<?php the_permalink(); ?>" class="btn btn-primary main-link">Learn More</a>
  <?php } else { ?>
    <a href="<?php the_permalink(); ?>" class="btn btn-primary main-link">Read More</a>
  <?php } ?>
  <div class="clearfix"></div>
</article>
