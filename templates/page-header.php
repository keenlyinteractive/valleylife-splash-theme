<?php use Roots\Sage\Titles; ?>

<div class="page-header" <?php if(get_post_meta($post->ID, '_page_options_title_image', true)) { echo 'style="background-image: url(' . get_post_meta($post->ID, '_page_options_title_image', true) . ')"'; }?>>
  <h1><?= Titles\title(); ?></h1>
</div>
