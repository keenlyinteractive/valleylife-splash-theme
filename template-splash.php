<?php
/**
 * Template Name: Splash Template
 */
?>

<div class="hero splash">
	<h1>Make Disciples.<br>Plant Churches.</h1>
</div>

<section>
	<h2>Connect with Us</h2>
	<p>We are a network of churches, each with its own pastor and leaders. We share a common culture, teaching and administrative resources, and a desire to see the gospel change our local communities.</p>
</section>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>

<section class="churches arrow-down">
	<ul class="list-unstyled list-inline animate-section">
		<li>
			<img src="<?php echo get_template_directory_uri ()?>/dist/images/VLC_Main_Tramonto.jpg">
			<div class="church-info">
				<h3>Tramonto</h3>
				<p><strong>Sundays</strong> 9 and 10:30 am<br><strong>Students</strong> Sundays at 4 pm<br>Weekly Community Groups</p>
				<p>34406 North 27th Drive<br>Phoenix, AZ 85085</p>
				<p><a href="http://tramonto.valleylife.church/location/" class="text-lg">Get Directions</a></p>
			</div>
			<a href="http://tramonto.valleylife.church" class="church-link">Choose Tramonto</a>
		</li> 
		<li>
			<img src="<?php echo get_template_directory_uri ()?>/dist/images/VLC_Main_Surprise.jpg">
			<div class="church-info">
				<h3>Surprise</h3>
				<p><strong>Sundays</strong> 10:00 am<br><strong>Students</strong> Sundays at 4 pm<br>Weekly Community Groups</p>
				<p>17901 W Lundberg St, <br/>Surprise, AZ 85388</p>
				<p><a href="http://surprise.valleylife.church/location/" class="text-lg">Get Directions</a></p>
			</div>
			<a href="http://surprise.valleylife.church" class="church-link">Choose Surprise</a>
		</li>
		<li>
			<img src="<?php echo get_template_directory_uri ()?>/dist/images/VLC_Main_Arrowhead.jpg">
			<div class="church-info">
				<h3>Arrowhead</h3>
				<p><strong>Launching January 2017</strong>, currently meeting Sundays with Valley Life Tramonto<br>Weekly Community Groups</p>
				<p>Future Services to be Held at<br/>Arrowhead Elementary School</p>
				<p><a href="http://arrowhead.valleylife.church/location/" class="text-lg">Get Directions</a></p>
			</div>
			<a href="http://arrowhead.valleylife.church" class="church-link">Choose Arrowhead</a>
		</li>
	</ul>
</section>