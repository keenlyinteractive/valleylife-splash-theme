<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <div class="wrap container" role="document">
      <div class="content row">
        <div class="col-sm-12">
          <div class="page-header" <?php if(get_post_meta($post->ID, '_page_options_title_image', true)) { echo 'style="background-image: url(' . get_post_meta($post->ID, '_page_options_title_image', true) . ')"'; }?>>
            <h1><?php the_title() ?></h1>
          </div>
        </div>
        <main class="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if (Setup\display_sidebar()) : ?>
          <aside class="sidebar">
            <?php include Wrapper\sidebar_path(); ?>


            <div class="sidebar-next-step">
              <h3>Your next step</h3>
              <?php
              $args = array( 'post_type' => 'events', 'posts_per_page' => 1 );
              $myposts = get_posts( $args );
              foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
                <div>
                  <?php the_post_thumbnail(); ?>
                  <div class="event-info">
                    <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
                    <p><?php the_excerpt(); ?></p>
                  </div>
                  <div class="event-info-bottom">
                    <?php

                    $ts = get_post_meta($post->ID, '_event_details_date', true);
                    $date = new DateTime("@$ts");
                    ?>
                    <span class="event-date"><?php echo $date->format('l, M j, g:ia') . "\n"; ?></span>
                    <a href="<?php the_permalink(); ?>" class="event-link"><?php echo get_post_meta($post->ID, '_event_details_link_text', true); ?></a>
                  </div>
                </div>
              <?php endforeach;
              wp_reset_postdata();?>
            </div>


          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>
